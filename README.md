# Omnifi Articles

The articles (long-form posts) published on the [Omnifi](https://omnifi.foundation) web site and used for syndication.